-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 01, 2019 at 04:08 PM
-- Server version: 5.7.26-0ubuntu0.18.10.1
-- PHP Version: 7.2.17-0ubuntu0.18.10.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `auction`
--

-- --------------------------------------------------------

--
-- Table structure for table `auction`
--

CREATE TABLE `auction` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `start_time` varchar(20) NOT NULL,
  `end_time` varchar(20) NOT NULL,
  `location` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `sub_category` int(11) NOT NULL,
  `price` varchar(10) NOT NULL,
  `email` varchar(150) NOT NULL,
  `image_1` varchar(150) DEFAULT NULL,
  `image_2` varchar(150) DEFAULT NULL,
  `image_3` varchar(150) DEFAULT NULL,
  `image_4` varchar(150) DEFAULT NULL,
  `image_5` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auction`
--

INSERT INTO `auction` (`id`, `title`, `description`, `date`, `start_time`, `end_time`, `location`, `category`, `sub_category`, `price`, `email`, `image_1`, `image_2`, `image_3`, `image_4`, `image_5`) VALUES
(9, 'Where can I get some?', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2019-05-31 10:06:44', '16:30', '17:05', 3, 2, 2, '40000', 'thanos@titan.com', 'bd9a0bac47341f2e94498e3b22b023ee.jpg', 'e05ee2802231f4f03ea2037977620bb5.jpg', '2da20a95edeb79123ddb7c0c3ab8f756.jpg', '', ''),
(10, 'What is lorem ipsum?', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2019-05-31 10:06:44', '16:30', '17:05', 3, 2, 2, '40000', 'thanos@titan.com', 'bd9a0bac47341f2e94498e3b22b023ee.jpg', 'e05ee2802231f4f03ea2037977620bb5.jpg', '2da20a95edeb79123ddb7c0c3ab8f756.jpg', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `bids`
--

CREATE TABLE `bids` (
  `id` int(11) NOT NULL,
  `amount` varchar(20) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `auction` int(11) NOT NULL,
  `bidder` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bids`
--

INSERT INTO `bids` (`id`, `amount`, `time`, `auction`, `bidder`) VALUES
(1, '100', '2019-05-31 07:09:49', 8, 'johndoe@email.com'),
(2, '100', '2019-05-31 07:09:49', 8, 'johndoe@email.com'),
(3, '100', '2019-05-31 07:09:49', 8, 'johndoe@email.com'),
(4, '100', '2019-05-31 07:09:49', 8, 'johndoe@email.com'),
(5, '100', '2019-05-31 07:09:49', 8, 'johndoe@email.com'),
(6, '100', '2019-05-31 07:09:49', 8, 'johndoe@email.com'),
(7, '100', '2019-05-31 07:09:49', 8, 'johndoe@email.com'),
(8, '100', '2019-05-31 07:09:49', 8, 'johndoe@email.com'),
(9, '100', '2019-05-31 07:09:49', 8, 'johndoe@email.com'),
(10, '100', '2019-05-31 07:09:49', 8, 'johndoe@email.com'),
(11, '100', '2019-05-31 07:09:49', 8, 'johndoe@email.com'),
(12, '', '2019-05-31 07:09:49', 1, 'johndoe@email.com'),
(13, '4000', '2019-05-31 07:09:49', 1, 'johndoe@email.com'),
(14, '5000', '2019-05-31 10:27:46', 9, 'thanos@titan.com'),
(15, '5010', '2019-05-31 10:40:34', 9, 'thanos@titan.com'),
(16, '8000', '2019-05-31 10:40:39', 9, 'thanos@titan.com'),
(17, '1000', '2019-05-31 10:40:45', 9, 'thanos@titan.com');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Land'),
(2, 'Real Estate');

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `name`) VALUES
(1, 'Zoo Road tinial'),
(2, 'Bhangagarh'),
(3, 'somewhere');

-- --------------------------------------------------------

--
-- Table structure for table `personal_details`
--

CREATE TABLE `personal_details` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `street` varchar(150) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `alt_phone` varchar(15) NOT NULL,
  `zip_code` int(7) NOT NULL,
  `email` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `personal_details`
--

INSERT INTO `personal_details` (`id`, `name`, `street`, `phone`, `alt_phone`, `zip_code`, `email`) VALUES
(1, 'John doe', 'Somewhere', '9876543210', '', 781008, 'johndoe@email.com'),
(2, 'Thanos  ', 'Street-1, Sector-1, Titan', '9876543210', '', 0, 'thanos@titan.com');

-- --------------------------------------------------------

--
-- Table structure for table `sub_category`
--

CREATE TABLE `sub_category` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `category` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_category`
--

INSERT INTO `sub_category` (`id`, `name`, `category`) VALUES
(1, 'Housing', 2),
(2, 'Apartment', 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(256) NOT NULL,
  `last_password` varchar(256) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `last_password`, `status`, `created_at`, `updated_at`) VALUES
(1, 'johndoe@email.com', '123', '123', 'active', '2019-05-31 07:11:42', NULL),
(4, 'thanos@titan.com', '$2y$10$Z8rSQtEou2hO6jfoMnzEmOPd6mbpPDSVakLt99dDztZfpp6erL0xW', '$2y$10$Z8rSQtEou2hO6jfoMnzEmOPd6mbpPDSVakLt99dDztZfpp6erL0xW', 'active', '2019-05-31 08:22:50', NULL),
(5, '', '$2y$10$z6gCWrW7hZ3HoB0tlUG0wefnsyongG/FH85N7EuIeu44eFmbQuL5a', '$2y$10$z6gCWrW7hZ3HoB0tlUG0wefnsyongG/FH85N7EuIeu44eFmbQuL5a', 'active', '2019-06-01 07:23:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `winner`
--

CREATE TABLE `winner` (
  `id` int(11) NOT NULL,
  `auction` int(11) NOT NULL,
  `bid` int(11) NOT NULL,
  `winner` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `winner`
--

INSERT INTO `winner` (`id`, `auction`, `bid`, `winner`) VALUES
(1, 9, 16, 'thanos@titan.com'),
(2, 9, 16, 'thanos@titan.com'),
(3, 9, 16, 'thanos@titan.com'),
(4, 9, 16, 'thanos@titan.com'),
(5, 9, 16, 'thanos@titan.com'),
(6, 9, 16, 'thanos@titan.com'),
(7, 9, 16, 'thanos@titan.com'),
(8, 9, 16, 'thanos@titan.com'),
(9, 9, 16, 'thanos@titan.com'),
(10, 9, 16, 'thanos@titan.com'),
(11, 9, 16, 'thanos@titan.com'),
(12, 9, 16, 'thanos@titan.com'),
(13, 9, 16, 'thanos@titan.com'),
(14, 9, 16, 'thanos@titan.com'),
(15, 9, 16, 'thanos@titan.com'),
(16, 9, 16, 'thanos@titan.com'),
(17, 9, 16, 'thanos@titan.com'),
(18, 9, 16, 'thanos@titan.com'),
(19, 9, 16, 'thanos@titan.com'),
(20, 9, 16, 'thanos@titan.com'),
(21, 9, 16, 'thanos@titan.com'),
(22, 9, 16, 'thanos@titan.com'),
(23, 9, 16, 'thanos@titan.com'),
(24, 9, 16, 'thanos@titan.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auction`
--
ALTER TABLE `auction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bids`
--
ALTER TABLE `bids`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personal_details`
--
ALTER TABLE `personal_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_category`
--
ALTER TABLE `sub_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `winner`
--
ALTER TABLE `winner`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auction`
--
ALTER TABLE `auction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `bids`
--
ALTER TABLE `bids`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `personal_details`
--
ALTER TABLE `personal_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sub_category`
--
ALTER TABLE `sub_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `winner`
--
ALTER TABLE `winner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
